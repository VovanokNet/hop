using System;
using System.Collections.Generic;
using UnityEngine;

public class GameEnvironment : MonoBehaviour
{
	[SerializeField] private GameObject platformPrefab;
	[SerializeField] private float zCoordInstantiate;
	[SerializeField] private float zCoordDestroy;
	[SerializeField] private float zCoordPlayer;
	[SerializeField] private float zDelta;

	[SerializeField] private Ball player;

	private const int COUNT_LINES = 4;

	private int lineNumber;

	private List<Platform> platforms = new List<Platform>();
	private Platform lastPlatform;

	private void Start()
	{
		lineNumber = UnityEngine.Random.Range(0, COUNT_LINES);

		player.transform.position = new Vector3(GetXbyLineIndex(lineNumber), player.transform.position.y, player.transform.position.z);

		float yCoord = zCoordPlayer;
		while (yCoord <= zCoordInstantiate)
		{
			CreatePlatform(yCoord);
			yCoord += zDelta;
			NextLine();
		}
	}

	private void Update()
	{
		if (lastPlatform != null)
		{
			if (Mathf.Abs(lastPlatform.transform.position.z - zCoordInstantiate) > zDelta)
			{
				NextLine();
				CreatePlatform(zCoordInstantiate);
			}
		}
	}

	private void NextLine()
	{
		var changeLineDirection = UnityEngine.Random.Range(0, 3);

		if (changeLineDirection == 0) //To left
			lineNumber += lineNumber > 0 ? -1 : 1;
		else if (changeLineDirection == 2) //To right
			lineNumber += lineNumber < COUNT_LINES - 1 ? 1 : -1;
	}

	private float GetXbyLineIndex(int lineIndex)
	{
		switch (lineIndex)
		{
			case 0: return -12;
			case 1: return -4;
			case 2: return 4;
			default: return 12;
		}
	}

	private void CreatePlatform(float z)
	{
		var platformGo = Instantiate(
			platformPrefab,
			new Vector3(GetXbyLineIndex(lineNumber), 0, z),
			platformPrefab.transform.rotation) as GameObject;

		platformGo.transform.SetParent(transform);

		if (platformGo != null)
		{
			lastPlatform = platformGo.GetComponent<Platform>();
			lastPlatform.BorderForDie = zCoordDestroy;
			lastPlatform.OnDie += PlatformDie;
			platforms.Add(lastPlatform);
		}
	}

	private void PlatformDie(Platform platform)
	{
		platform.OnDie -= PlatformDie;
		platforms.Remove(platform);
		Destroy(platform.gameObject);
	}
}
