﻿using System;
using UnityEngine;

public class Platform : MonoBehaviour
{
	public const float SPEED = 5f / 2.04f + 0.04f;

	public float BorderForDie { get; set; }

	public event Action<Platform> OnDie;

	private void Update()
	{
		transform.Translate(-Vector3.forward * Time.deltaTime * SPEED);

		if (transform.position.z <= BorderForDie && OnDie != null)
			OnDie(this);
	}
}
