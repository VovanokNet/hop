using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
	[SerializeField] private float speedMoveSide;
	[SerializeField] private Text scoreText;
	[SerializeField] private GameObject failPanel;

	private Rigidbody selfRigidBody;

	private bool isMoveLeft;
	private bool isMoveRight;
	private float startMovedX;

	private void Start()
	{
		selfRigidBody = GetComponent<Rigidbody>();
	}

	private void OnCollisionEnter()
	{
		selfRigidBody.AddForce(Vector3.up * 10, ForceMode.Impulse);
		scoreText.text = (int.Parse(scoreText.text) + 1).ToString();
	}

	private void Update()
	{
		if (isMoveRight || isMoveLeft)
		{
			transform.Translate((isMoveRight ? Vector3.right : Vector3.left) * Time.deltaTime * speedMoveSide);
			var distanceFromStart = Mathf.Abs(transform.position.x - startMovedX);
			if (distanceFromStart >= 8f)
			{
				transform.Translate((isMoveRight ? Vector3.left : Vector3.right) * Mathf.Clamp01(distanceFromStart - 8f));
				isMoveRight = false;
				isMoveLeft = false;
			}
		}

		if (transform.position.y < 0)
			failPanel.SetActive(true);
	}

	public void MoveLeft()
	{
		if (isMoveLeft || isMoveRight)
			return;

		isMoveLeft = true;
		startMovedX = transform.position.x;
	}

	public void MoveRight()
	{
		if (isMoveLeft || isMoveRight)
			return;

		isMoveRight = true;
		startMovedX = transform.position.x;
	}
}
